# Introduction
Module to configure backups.
You can select a resource to backup with `selection tags` or selecting the `resources`.

# Example
```
  vault_name = "itc-shetrades-platform-infra"
  backup_plan_name = "d1w_w2w"
  backup_plan_rules = [{
    name = "d1w"
    resources = []
    selection_tag = [{
      type  = "STRINGEQUALS"
      key   = "Name"
      value = "itc-shetrades-platform-infra"
    }]
    schedule           = "cron(0 21 ? * MON-SAT *)"
    start_window       = 60
    completion_window  = 240
    delete_after       = 6
  },{
    name = "w2w"
    resources = []
    selection_tag = [{
      type  = "STRINGEQUALS"
      key   = "Name"
      value = "itc-shetrades-platform-infra"
    }]
    schedule           = "cron(0 21 ? * SUN *)"
    start_window       = 60
    completion_window  = 240
    delete_after       = 15
  }]
```