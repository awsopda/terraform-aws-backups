variable "enable_backup" {
  type        = bool
  description = "Enable backup true creates all resources associated to this module"
  default     = true
}

variable "region" {
  description = "Region where the backups will be configured"
  type        = string
  default     = "eu-west-1"
}

variable "vault_name" {
  type        = string
  description = "Solution name, e.g. 'app' or 'cluster'"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}

variable "kms_key_arn" {
  type        = string
  description = "The server-side encryption key that is used to protect your backups"
  default     = null
}

variable "backup_plan_name" {
  description = "Plan name"
  type        = string
  default     = "d1w_w2w"
}

variable "backup_plan_rules" {
  description = "Backups plan to be created and store their backups in the vault"
  type        = list(object({
    name              = string
    resources         = list(string)
    selection_tag     = list(map(string))
    schedule          = string
    start_window      = number
    completion_window = number
    delete_after      = number
  }))
  default = [{
    name = "d1w"
    resources = []
    selection_tag = []
    schedule           = "cron(0 21 ? * MON-SAT *)"
    start_window       = 60
    completion_window  = 240
    delete_after       = 6
  },{
    name = "w2w"
    resources = []
    selection_tag = []
    schedule           = "cron(0 21 ? * SUN *)"
    start_window       = 60
    completion_window  = 240
    delete_after       = 15
  }]
}