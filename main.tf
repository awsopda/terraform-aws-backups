resource "aws_backup_vault" "default" {
  count = var.enable_backup ? 1 : 0

  name        = var.vault_name
  kms_key_arn = var.kms_key_arn
  tags        = var.tags
}

resource "aws_backup_plan" "default" {
  count = var.enable_backup ? 1 : 0

  name  = "${var.vault_name}-${var.backup_plan_name}"
  tags  = var.tags

  dynamic "rule" {
    for_each = var.backup_plan_rules
    content {
      rule_name           = rule.value.name
      target_vault_name   = aws_backup_vault.default[0].name
      schedule            = rule.value.schedule
      start_window        = rule.value.start_window
      completion_window   = rule.value.completion_window
      recovery_point_tags = var.tags

      dynamic "lifecycle" {
        for_each = rule.value.delete_after != null ? ["true"] : []
        content {
          delete_after = rule.value.delete_after
        }
      }
    }
  }
}

data "aws_iam_policy_document" "assume_role" {
  count = var.enable_backup ? 1 : 0

  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "default" {
  count = var.enable_backup ? 1 : 0

  name               = "${var.vault_name}-backup-role"
  assume_role_policy = join("", data.aws_iam_policy_document.assume_role.*.json)
  tags               = var.tags
}

resource "aws_iam_role_policy_attachment" "default" {
  count = var.enable_backup ? 1 : 0

  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = join("", aws_iam_role.default.*.name)
}

resource "aws_backup_selection" "default" {
  count        = var.enable_backup ? length(var.backup_plan_rules) : 0
  name         = "${var.vault_name}-${var.backup_plan_rules[count.index].name}"
  iam_role_arn = join("", aws_iam_role.default.*.arn)
  plan_id      = aws_backup_plan.default[0].id
  resources    = var.backup_plan_rules[count.index].resources

  dynamic "selection_tag" {
    for_each = var.backup_plan_rules[count.index].selection_tag
    content {
      type  = selection_tag.value.type
      key = selection_tag.value.key
      value  = selection_tag.value.value
    }
  }
}
